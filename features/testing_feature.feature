Feature: testing feature
  <Some interesting description here>

  Scenario:
    <Some interesting scenario steps here>
    Given aeSite header set as <aeSite>
And user type <userType>
When I add 'commerce' item 'without promotion' with quantity 9
Then there are no errors and status is success
When I add shipping address to cart: commerce/data/common/shippingAddress/<shippingAddress>
Then there are no errors and status is success
When I add gift card 'Gift_Card_75_USD_01'
Then there are no errors and status is success
When I add gift card 'Gift_Card_25_USD_02'
Then there are no errors and status is success
When I add gift card 'Gift_Card_25_USD_03'
Then there are no errors and status is success
When I add gift card 'Gift_Card_25_USD_01'
Then there are the following errors in response:
|fields    |key                                     |
|[giftCard]|error.checkout.payment.giftCard.exceeded|